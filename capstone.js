2. Create a query that inserts a new item

db.items.insert({
	name: "Corned Beef",
	tags: ['Food', 'Cooking', 'Seasoning'],
	stock_count: 70,
	stock_threshold: 30,
	logs: [
		{
			user: "Jaff",
			description: 'Jaff added 7 stocks of Corned Beef',
			date: '03/24/2020'
		},
		{
			user: "Fe",
			description: 'Fe added 5 stocks of Corned Beef',
			date: '09/15/2020'
		},
		{
			user: "Benne",
			description: 'Benne added 9 stocks of Corned Beef',
			date: '05/11/2020'
		}
	],
	isOnhold: false

})


3. Put all the items that reached the threshold to onHold

db.items.updateMany(
	{
		$expr: {$gte: ['$stock_threshold','$stock_count'] } 
	},
	{
		$set: {is_Onhold: true}
	}
)


4. Remove all onhold items with 0 stock

db.items.deleteMany(

	{
		stock_count: 0
	},
	{

	}
)

5. Get all items that reached the threshold or items that are havent reached the threshold but is withheld

db.items.find({
	$or:[ {$expr: {$lte: ['stock_count', 'stock_threshold']}} , { $and: [{$expr: { $gt: ['stock_count', 'stock_threshold']}}, {is_Onhold: true} ]  } ]
}).pretty()


//stretch goals:

1. Create a query that inserts a log in an item.

db.items.update(
	{
		_id: ObjectId("5ec22dbaf3f17674b46b1ec2")
	},
	{
		$push: { logs: {user: 'Mark', description: 'Mark added 7 stocks of Corned Beef', date: '5/10/20'} }
	}

) 

2. Create a query that adds multiple tags to an item

db.items.update(
	{
		_id: ObjectId("5ec22dbaf3f17674b46b1ec2")
	},
	{
		$push: {tags: {$each: ['Frozen', 'Soap']}}
	}
)

3. Create a query that gets all the item in a provided list of tags i.e: Food, Cooking, and Seasoning

db.items.find({tags:{ $in: ['Food','Cleaning', 'Accessory']} }).pretty()

